const Sequelize = require('sequelize')
const db = require('../database/db')

module.exports = db.sequelize.define(
  'buget',
  {
    company: {
      type: Sequelize.STRING
    },
    bugetID: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    userID: {
      type: Sequelize.INTEGER,
    },
    doctID: {
      type: Sequelize.INTEGER
    },
    status: {
        type: Sequelize.INTEGER
    },
    approvedDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    ncitas: {
        type: Sequelize.INTEGER
    },
    fcitas: {
      type: Sequelize.INTEGER
    },
    rteInfe: {
      type: Sequelize.DECIMAL(10,3),
    },
    rteSupe: {
      type: Sequelize.DECIMAL(10,3)
    },
    blanqui: {
      type: Sequelize.DECIMAL(10,3)
    },
    price: {
        type: Sequelize.DECIMAL(10,3),
        defaultValue: 0
    },
    note: {
      type: Sequelize.STRING
    },
    photos: {
      type: Sequelize.STRING
    },
    updatedAt:{
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false
    },
    createdAt:{
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    }
  },
  {
    timestamps: true,
    freezeTableName: true,
    hooks: {

    }
  }
)