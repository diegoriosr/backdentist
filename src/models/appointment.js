const Sequelize = require('sequelize')
const db = require('../database/db')

module.exports = db.sequelize.define(
  'appointment',
  {
    appointID: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    company: {
      type: Sequelize.STRING
    },
    bugetID: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    userID: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    doctID: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    status: {
        type: Sequelize.INTEGER
    },
    order: {
      type: Sequelize.INTEGER
    },
    note: {
      type: Sequelize.STRING
    },
    photos: {
      type: Sequelize.STRING
    },
    approvedDate: {
        type: Sequelize.DATE,
        allowNull: false
    },
    reschedule: {
        type: Sequelize.INTEGER
    },
    updatedAt:{
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false
    },
    createdAt:{
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    }
  },
  {
    timestamps: true,
    freezeTableName: true,
    hooks: {

    }
  }
)