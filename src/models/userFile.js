const Sequelize = require('sequelize')
const bcrypt = require("bcryptjs")
const db = require('../database/db')

module.exports = db.sequelize.define(
  'userFile',
  {
    userID: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    company: {
      type: Sequelize.STRING,
      unique: true
    },
    resaleID: {
      type: Sequelize.STRING,
      unique: true
    },
    userName: {
      type: Sequelize.STRING
    },
    lastName: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING,
      unique: false
    },
    phone: {
      type: Sequelize.STRING
    },
    city: {
      type: Sequelize.STRING
    },
    adress: {
      type: Sequelize.STRING
    },
    age: {
      type: Sequelize.STRING
    },
    role: {
      type: Sequelize.STRING
    },
    pathURL: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.STRING,
      defaultValue: 0
    },
    authCode: {
      type: Sequelize.STRING,
      defaultValue: 0
    },
    register: {
      type: Sequelize.STRING,
      defaultValue: 0
    },
    consultingrooms: {
      type: Sequelize.INTEGER,
    },
    fcmToken: {
      type: Sequelize.STRING,
    },
    comment: {
       type: Sequelize.STRING
    },
    updatedAt:{
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false
    },
    createdAt:{
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    }
  },
  {
    timestamps: true,
    freezeTableName: true,
    hooks: {
      beforeCreate: (userFile) => {
        if( userFile.password != "" && userFile.password != null ) 
        {
          const salt = bcrypt.genSaltSync(10);
          userFile.password = bcrypt.hashSync(userFile.password, salt);
        }
      }
    }
  }
)