const Sequelize = require('sequelize');
const db = {}
const sequelize = new Sequelize(process.env.DATABASE, process.env.USERDB, process.env.PASSDB, {
   host: "107.161.188.74",
   dialect: 'mysql',
   operatorAliases: false,
   timezone: "-05:00",
   logging: false,
   pool:{
     max: 10,
     min: 0,
     acquire: 30000,
     idle: 10000
   }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
