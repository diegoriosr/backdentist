require('dotenv').config();
const express = require('express')
var helmet = require('helmet')
const fileUpload = require('express-fileupload')

const path = require('path')
const cors = require('cors')

const app = express()

//settings
app.set('port', process.env.PORT || 3500)

//middlewares
//app.disable('x-powered-by')
app.use(helmet())
app.use(cors())
app.use(express.json()) //body-parse
app.use(express.urlencoded({extended: false}));
app.use(fileUpload())

// static files
app.use("*/img", express.static(path.join(__dirname,'public','img')));
app.use("*/citas", express.static(path.join(__dirname,'public','citas')));
app.use("*/ppto", express.static(path.join(__dirname,'public','ppto')));
app.use("*/base", express.static(path.join(__dirname,'public','base')));
app.use("*/site", express.static(path.join(__dirname,'public','bootstrap')));

// Variables Globlales

//Routes
app.get('/', (req, res) => { res.redirect('http://127.0.0.1:3500/site/') });
app.use(require('./routes/user.routes'))
app.use(require('./routes/buget.routes'))

async function main() {
 await app.listen(app.get('port'), () => {  });
 console.log("server on port " + app.get('port'))
}

main()