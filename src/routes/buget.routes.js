const router = require("express").Router();

const {
    renderGetNew,
    renderUpload,
    renderUploadPpto,
    renderBugetByID,
    renderBugetGetByID,
    renderApprove,
    getAllApprove,
    getNewAppoint,
    getAllAppoint,
    getAprovAppoint,
    getAllAgenda,
    getSaveAppoint,
    getSaveBuget,
    getAllIncumplidos,
    getBugetNextDate,
} = require("../controllers/buget.controller")

router.post("/buget/newBuget", renderGetNew)

router.get("/buget/getByID", renderBugetGetByID)

router.get("/buget/geBugetID", renderBugetByID)

router.post("/buget/getApprove", renderApprove)

router.get("/buget/getAllApprove", getAllApprove)

router.post("/buget/upload", renderUpload)

router.post("/apponint/getNewAppoint", getNewAppoint)

router.get("/apponint/getAllAppoint", getAllAppoint)

router.post("/apponint/getAprovAppoint", getAprovAppoint)

router.post("/apponint/getSaveAppoint", getSaveAppoint)

router.post("/apponint/getSaveBuget", getSaveBuget)

router.get("/apponint/getAllAgenda", getAllAgenda)

router.get("/apponint/getBugetNextDate", getBugetNextDate)

router.get("/apponint/getAllIncumplidos", getAllIncumplidos)

router.post("/apponint/renderUpload", renderUpload)

router.post("/apponint/renderUploadPpto", renderUploadPpto)

module.exports = router;