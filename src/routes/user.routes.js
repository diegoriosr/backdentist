const router = require("express").Router();

const {
    renderGetNew,
    renderGetNewDoc,
    renderPassword,
    renderGetNot,
    renderLogin,
    renderUpload,
    renderUploadMark,
    renderGetAllPatient,
    renderGetByIDPatient,
    renderGetByToken,
    sendNotification
} = require("../controllers/user.controller")

router.get("/user/sendNotif", sendNotification)

router.post("/user/signin", renderGetNew)

router.post("/user/signup", renderGetNewDoc)

router.post("/user/password", renderPassword)

router.post("/user/upload", renderUpload)

router.post("/user/uploadMark", renderUploadMark)

router.post("/user/login", renderLogin)

router.get("/user/getAllPatient", renderGetAllPatient)

router.get("/user/getByIDPatient", renderGetByIDPatient)

router.post("/user/GetByToken", renderGetByToken)

router.get("/user/notify", renderGetNot)


module.exports = router;