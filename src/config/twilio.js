const twilio = require('twilio');
const { ModelBuildContext } = require('twilio/lib/rest/autopilot/v1/assistant/modelBuild');

var accountSid = process.env.TWILIO_ACCOUNT_SID; // Your Account SID from www.twilio.com/console
var authToken = process.env.TWILIO_AUTH_TOKEN;   // Your Auth Token from www.twilio.com/console

const client = twilio(accountSid, authToken, {
    lazyLoading: true
});

module.exports = client