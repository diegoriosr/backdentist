$(function() {
    $("#btnSave").click((e) => {
        e.preventDefault();        
        $.ajax({
          method: "POST",
          url: "/user/signup",
          data: $('#frmDoctor').serialize(),
          dataType: "json",
          success: function(json) 
          {
              if( json.status ) 
              {
                var formData = new FormData();
                formData.append('file', $('#file')[0].files[0]);
                formData.append('company', $('#company').val());

                $.ajax({
                    method: "POST",
                    url: "/user/uploadMark",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(jsons)  {
                        alert(json.msg)
                        location.reload()
                    }
                  });
              }
          }
        });
    })
});