const path = require('path')
const Op = require('sequelize').Op
const {
	mkdirSync,
	existsSync
} = require('fs');

const bugetCtrl = {}

const locale =  {
	hour12 : true,
	weekday: 'long',
	day : 'numeric',
	month : 'long',
	year : 'numeric',
	hour: '2-digit',
	minute: '2-digit',
	timeZone: 'America/Bogota',
	//timeZoneName: 'short'
}

const Week = { Monday:"Lunes", Tuesday:"Martes", Wednesday:"Miércoles", Thursday:"Jueves", Friday:"Viernes", Saturday:"Sábado", Sunday:"Domingo" }
const Month = {January:"Enero", February:"Febrero", March:"Marzo", April:"Abril", May:"Mayo", June:"Junio", July:"Julio", August:"Agosto", September:"Septiembre", October:"Octubre", November:"Noviembre", December:"Diciembre"}

//models
const buget = require("../models/buget")
const appoi = require("../models/appointment");

// Modules

bugetCtrl.getAllAgenda = (req, res) => {

	var errors = []

	const { doctID, company, pDate } = req.query

	if (!doctID || !company) {
		errors.push({ data: 'Ingrese los campos requeridos' });
	}

	if (errors.length > 0)
		res.status(500).json({ data: errors })
	else {
		buget.sequelize.query(`SELECT ncitas,fcitas,rteSupe,rteInfe,blanqui,
								   	  b.*,userName,lastName,pathURL
							   FROM appointment b inner Join buget bu using( bugetID, userID, doctID )
													inner Join userFile u using(userID)
							   Where b.company = '${company}' 
							         and b.doctID = ${doctID}
							         and (
										( 
										b.status = 2 
											and 
										( b.approvedDate >= '${pDate}' And Convert(b.approvedDate, Date) = Convert('${pDate}', Date) ) 
										)
										Or
										(
											b.status = 4
										)
									 )
							   Order By b.approvedDate;`,
		{ type: buget.sequelize.QueryTypes.SELECT })
		.then((data) => {
			res.status(200).json({ data, fecha: pDate });
		})
		.catch(err => {
			res.status(500).json({ data: err });
		})
	}
}

bugetCtrl.getAllIncumplidos = (req, res) => {

	var errors = []

	const { doctID, company } = req.query

	if (!doctID || !company) {
		errors.push({ data: 'Ingrese los campos requeridos' });
	}

	if (errors.length > 0)
		res.status(500).json({ data: errors })
	else {
		appoi.sequelize.query(`SELECT ncitas,fcitas,rteSupe,rteInfe,blanqui,
								   	  b.*,userName,lastName,pathURL,fcmToken
							   FROM appointment b inner Join buget bu using( bugetID, userID, doctID )
													inner Join userFile u using(userID)
							   Where b.status IN (2,4) and b.company = '${company}' and b.doctID = '${doctID}' And b.approvedDate <= NOW() 
							   Order By b.approvedDate;`,
		{ type: appoi.sequelize.QueryTypes.SELECT })
		.then((data) => {
			res.status(200).json({ data: data });
		})
		.catch(err => {
			res.status(500).json({ data: err });
		})
	}
}

bugetCtrl.getAprovAppoint = (req, res) => {

	var errors = []

	const { userID, doctID, bugetID, company, appointID, status } = req.body

	if (!userID || !doctID || !bugetID || !company) {
		errors.push({ data: 'Ingrese los campos requeridos' });
	}

	if (errors.length > 0)
		res.status(500).json({ data: errors })
	else {
		appoi.update({ status }, { where: { userID, doctID, bugetID, company, appointID } } )
		.then((rp) => {
			res.status(200).json({ data: "Cita aprobado con exito" });
		})
		.catch(err => {
			res.status(500).json({ data: err });
		})
	}
}

bugetCtrl.getSaveAppoint = (req, res) => {

	var errors = []
	const { userID, doctID, bugetID, company, appointID, photos, note } = req.body

	if (!userID || !doctID || !bugetID || !company || !appointID) {
		errors.push({ data: 'Ingrese los campos requeridos' });
	}

	if (errors.length > 0)
		res.status(500).json({ data: errors })
	else {
		appoi.update({ photos, note }, { where: { userID, doctID, bugetID, company, appointID } } )
		.then((rp) => {
			res.status(200).json({ data: "Cambio realizado con exito" });
		})
		.catch(err => {
			res.status(500).json({ data: err });
		})
	}
}

bugetCtrl.getSaveBuget = (req, res) => {

	var errors = []
	const { userID, doctID, bugetID, company, photos, note } = req.body

	if (!userID || !doctID || !bugetID || !company) {
		errors.push({ data: 'Ingrese los campos requeridos' });
	}

	if (errors.length > 0)
		res.status(500).json({ data: errors })
	else {
		buget.update({ photos, note }, { where: { userID, doctID, bugetID, company } } )
		.then((rp) => {
			res.status(200).json({ data: "Fotos realizado con exito" });
		})
		.catch(err => {
			res.status(500).json({ data: err });
		})
	}
}

bugetCtrl.getAllAppoint = (req, res) => {

	var errors = []

	const { doctID, userID, company, bugetID, type } = req.query
	
	const Tipo = {'p':{ tipo:'patient', key:'userID'}, 'd': {tipo:'doctor', key:'doctID'}}

	if (!doctID || !company || !bugetID || !userID) {
		errors.push({ data: 'Ingrese los campos requeridos' });
	}

	if (errors.length > 0)
		res.status(500).json({ data: errors })
	else {
		/* appoi.findAll({
			attributes: ['bugetID', 'userID', 'doctID', 'status', 'order','note', 'photos',
			             [buget.sequelize.fn('date_format', buget.sequelize.col('createdAt'), '%Y-%m-%d'), 'createdAt'],
						 [buget.sequelize.fn('date_format', buget.sequelize.col('approvedDate'), '%Y-%m-%dT%H:%i:%s.122Z'), 'approvedDate'],
						],
			where: { company, userID, doctID, bugetID}
		}) */
		appoi.sequelize.query(`SELECT ncitas,fcitas,rteSupe,rteInfe,blanqui,
								   	  b.*,userName,lastName,pathURL,
									  (SELECT fcmToken FROM userFile Where company = b.company And userID = b.${Tipo[type].key} And role = '${Tipo[type].tipo}') AS fcmToken
							   FROM appointment b inner Join buget bu using( bugetID, userID, doctID )
													inner Join userFile u using(userID)
							   Where b.company = '${company}' and b.doctID = '${doctID}' and b.userID = '${userID}' and
							         b.bugetID = '${bugetID}'
							   Order By note,appointID,approvedDate asc;`,
		{ type: buget.sequelize.QueryTypes.SELECT })
		.then((data) => {
			res.status(200).json({ data: data });
		})
		.catch(err => {
			res.status(500).json({ data: err });
		})
	}
}

bugetCtrl.getNewAppoint = (req, res) => {

	var errors = []

	console.log( req.body );

	if (errors.length > 0)
		res.status(500).json({ data: errors })
	else 
	{
		appoi.bulkCreate(req.body, { 
			updateOnDuplicate: ['approvedDate', 'status', 'order', 'reschedule']
	    }).then((rp) => {
			console.log("Yes ", rp );
			res.status(200).json({ data: "Ppto registrado con exito" });
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({ data: err });
		})
	}
}

bugetCtrl.renderGetNew = (req, res) => {

	var errors = []

	const { userID, doctID, ncitas, company } = req.body

	if (!userID || !doctID || !ncitas || company == '' ) {
		errors.push({ data: 'Ingrese los campos requeridos' });
	}

	if (errors.length > 0)
		res.status(500).json({ data: errors })
	else {
		buget.upsert(req.body,{
            updateOnDuplicate: ['bugetID'],
            returning: ["bugetID"]
        })
		.then((rp) => {
			res.status(200).json({ data: "Ppto registrado con exito", id: rp[0].dataValues.bugetID });
		})
		.catch(err => {
			res.status(500).json({ data: err });
		})
	}
}

//(Paciente) Llamada Ppto por paciente próxima Cita
bugetCtrl.getBugetNextDate = (req, res) => {
	const tTor = []
	const { company, userID} = req.query

	appoi.findAll({
		attributes: ['bugetID', 'userID', 'doctID', 'order','note','approvedDate','status',
					 [buget.sequelize.fn('date_format', buget.sequelize.col('approvedDate'), '%Y-%m-%dT%H:%i:%s'), 'appDateTime'],					 
					 [buget.sequelize.literal('(SELECT ncitas FROM buget Where company = appointment.company And userID = appointment.userID And status = 1)'), 'ncitas'],
					 [buget.sequelize.literal('(SELECT fcmToken FROM userFile Where company = appointment.company And userID = appointment.doctID And role = "doctor")'), 'fcmToken']
					],
		where: { company, 
			     userID,
				 status: {
					[Op.in]: [1, 2, 4]
				 }
		}
	})
	.then(user => {

		user.forEach((value, index) => 
		{
			let weekdaye = new Date(value.dataValues.appDateTime).toLocaleString("en-US", {weekday: 'long'});
			let weekdays = Week[ weekdaye ];

			let monthe = new Date(value.dataValues.appDateTime).toLocaleString("en-US", {month: 'long'});
			let months = Month[ monthe ];

			tTor.push({
				key: index,
				bugetID: value.dataValues.bugetID,
				userID: value.dataValues.userID,
				doctID: value.dataValues.doctID,
				ncitas: value.dataValues.ncitas,
				order: value.dataValues.order,
				status: value.dataValues.status,
				approved: value.dataValues.approvedDate,
				appDateTime: new Date(value.dataValues.appDateTime).toLocaleString("en-US", locale).replace(weekdaye,weekdays).replace(monthe,months).toString(),
				icon: 'calendar',				
				fcmToken: value.dataValues.fcmToken
			})
		});

		if(tTor.length > 0) {
			res.status(200).json({ data: tTor })
		}
		else
			res.status(500).json({ data: "El cliente no tiene un presupuesto" })
		
	})
	.catch(err => {
		res.status(500).json({
			'data': err
		})
	})
}

//(Paciente) Llamada Ppto por paciente
bugetCtrl.renderBugetGetByID = (req, res) => {
	const tTor = []
	const { company, userID} = req.query

	buget.findAll({
		attributes: ['bugetID', 'userID', 'doctID', 'ncitas', 'fcitas', 'rteInfe', 'rteSupe', 'blanqui', 'price', 'status',
					 [buget.sequelize.fn('date_format', buget.sequelize.col('createdAt'), '%Y-%m-%d'), 'createdAt'],
					 [buget.sequelize.fn('date_format', buget.sequelize.col('approvedDate'), '%Y-%m-%d'), 'approvedDate']
					],
		where: { company, userID}
	})
	.then(user => {

		user.forEach((value, index) => 
		{
			tTor.push({
				key: index,
				bugetID: value.dataValues.bugetID,
				userID: value.dataValues.userID,
				doctID: value.dataValues.doctID,
				ncitas: value.dataValues.ncitas,
				fcitas: value.dataValues.fcitas,
				rteInfe: value.dataValues.rteInfe,
				rteSupe: value.dataValues.rteSupe,
				blanqui: value.dataValues.blanqui,
				price: value.dataValues.price,
				state: value.dataValues.status,
				created: value.dataValues.createdAt,
				approved: value.dataValues.approvedDate,
				icon: 'credit-card'
			})
		});

		if(tTor.length > 0) {
			res.status(200).json({ data: tTor })
		}
		else
			res.status(500).json({ data: "El cliente no tiene un presupuesto" })
		
	})
	.catch(err => {
		res.status(500).json({
			'data': err
		})
	})
}

//(Paciente / Doctor Carga Editar) Llama presupuestos Pendientes de Aprobación / para llenar Formulario de Calculo PPTO
bugetCtrl.renderBugetByID = (req, res) => {
	const tTor = []
	const { company, userID, doctID, bugetID, type} = req.query
	
	const Tipo = {'p':{tipo: 'patient', key: 'userID'}, 'd':{tipo: 'doctor', key: 'doctID'}}

	buget.findAll({
		attributes: ['bugetID', 'userID', 'doctID', 'ncitas', 'fcitas', 'rteInfe', 'rteSupe', 'blanqui', 'price', 'status','photos','note',
					 [buget.sequelize.fn('date_format', buget.sequelize.col('createdAt'), '%Y-%m-%d'), 'createdAt'],
					 [buget.sequelize.fn('date_format', buget.sequelize.col('approvedDate'), '%Y-%m-%d'), 'approvedDate'],
					 [buget.sequelize.literal('(SELECT fcmToken FROM userFile Where company = buget.company And userID = buget.'+Tipo[type].key+' And role = "'+Tipo[type].tipo+'")'), 'fcmToken']
					],
		where: { company, userID, 
			     doctID, bugetID,
				 status: {
					[Op.in]: type == "p" ? [0, 1] : [0]
				 },
			   }
	})
	.then(user => {
		user.forEach((value, index) => {
			tTor.push({
				key: index,
				bugetID: value.dataValues.bugetID,
				userID: value.dataValues.userID,
				doctID: value.dataValues.doctID,
				ncitas: value.dataValues.ncitas,
				fcitas: value.dataValues.fcitas,
				rteInfe: value.dataValues.rteInfe,
				rteSupe: value.dataValues.rteSupe,
				blanqui: value.dataValues.blanqui,
				photos: value.dataValues.photos,
				note: value.dataValues.note,
				price: value.dataValues.price,
				state: value.dataValues.status,
				created: value.dataValues.createdAt,
				approved: value.dataValues.approvedDate,
				fcmToken: value.dataValues.fcmToken,
				icon: 'av-timer'
			})
		});

		if(tTor.length > 0) {
			res.status(200).json({ data: tTor })
		}
		else
			res.status(500).json({ data: "No tiene presupuestos pendientes por aprobar" })
		
	})
	.catch(err => {
		res.status(500).json({
			'data': err
		})
	})
}

bugetCtrl.renderUpload = (req, res) => {

	let EDFile = req.files.file
	
	if (!existsSync(path.join(__dirname, '../public/img/', req.body.user))) {
		mkdirSync(path.join(__dirname, '../public/img/', req.body.user), {
			recursive: true
		})
	}

	EDFile.mv(`${path.join(__dirname,'../','/public/img/',req.body.user,'/')}${req.body.user}.png`, err => {
		if (err) return res.status(500).send({
			data: err
		})

		return res.status(200).send({
			data: 'File upload'
		})
	})
}

bugetCtrl.renderApprove = (req, res) => {

	var errors = []

	const { userID, doctID, bugetID } = req.body

	if (!userID || !doctID || !bugetID) {
		errors.push({ data: 'Ingrese los campos requeridos' });
	}

	if (errors.length > 0)
		res.status(500).json({ data: errors })
	else {
		buget.update({ status: 1, approvedDate: new Date() }, { where: { userID, doctID, bugetID } } )
		.then((rp) => {
			res.status(200).json({ data: "Presupuesto aprobado con exito" });
		})
		.catch(err => {
			res.status(500).json({ data: err });
		})
	}
}

bugetCtrl.getAllApprove = (req, res) => {

	var errors = []

	const { doctID, company } = req.query

	console.log( req.query );

	if (!doctID || !company) {
		errors.push({ data: 'Ingrese los campos requeridos' });
	}

	if (errors.length > 0)
		res.status(500).json({ data: errors })
	else {
		buget.sequelize.query(`SELECT bugetID,CONVERT(approvedDate, DATE) As approvedDate,
									  ncitas,fcitas,userID,
		                              userName,lastName,email,phone,pathURL,
		                              (SELECT fcmToken FROM userFile Where company = b.company And userID = b.userID And role = "patient") As 'fcmToken'
							   FROM buget b Inner Join userFile u using( userID )
							   Where b.status = 1 and b.company = '${company}' and b.doctID = ${doctID};`,
		{ type: buget.sequelize.QueryTypes.SELECT })
		.then((data) => {
			res.status(200).json({ data: data });
		})
		.catch(err => {
			res.status(500).json({ data: err });
		})
	}
}

bugetCtrl.renderUploadPpto = (req, res) => {

	let EDFile = req.files.file
	let dUser = JSON.parse(req.body.user)
	
	if (!existsSync( path.join(__dirname, '../public/ppto/', dUser.company, dUser.userName, String(dUser.bugetID) ) ) ) {
		mkdirSync( path.join(__dirname, '../public/ppto/', dUser.company, dUser.userName, String(dUser.bugetID) ), {
			recursive: true
		})
	}

	EDFile.mv(`${path.join(__dirname, '../public/ppto/', dUser.company, dUser.userName, String(dUser.bugetID), "/")}${req.body.timestamp}.png`, err => {
		if (err) return res.status(500).send({
			data: err
		})
		res.status(200).json({ data: "Archivo cargado con exito" });
	})
}

bugetCtrl.renderUpload = (req, res) => {

	let EDFile = req.files.file
	let dUser = JSON.parse(req.body.user)
	
	if (!existsSync( path.join(__dirname, '../public/citas/', dUser.company, dUser.userName, String(dUser.bugetID), String(dUser.appointID) ) ) ) {
		mkdirSync( path.join(__dirname, '../public/citas/', dUser.company, dUser.userName, String(dUser.bugetID), String(dUser.appointID) ), {
			recursive: true
		})
	}

	EDFile.mv(`${path.join(__dirname, '../public/citas/', dUser.company, dUser.userName, String(dUser.bugetID), String(dUser.appointID), "/")}${req.body.timestamp}.png`, err => {
		if (err) return res.status(500).send({
			data: err
		})
		res.status(200).json({ data: "Archivo cargado con exito" });
	})
}

module.exports = bugetCtrl;