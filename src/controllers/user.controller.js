const { nanoid } = require('nanoid')
const nodemailer = require('nodemailer')
const Op = require('sequelize').Op

const path = require('path')
const {
	mkdirSync,
	existsSync
} = require('fs');

const userCtrl = {}

//models
const User = require("../models/userFile")

// Modules
const twilio = require('../config/twilio')
const admin = require("firebase-admin");
const bcrypt = require("bcryptjs")

var serviceAccount = require("../config/dentispocket-firebase-adminsdk-gocey-52462dda37.json");

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount)
});

const enviar_mail = async(pnombre, code, email) => {
	
    let transporter = nodemailer.createTransport({
		service: 'gmail',
        host: "smtp.gmail.com",
        port: 587,
        secure: true,
        auth: {
            user: "notificacion-septiclean@fenix-awc.info",
            pass: "2021AWC-fenix*"
        }
    });

    let mail_options = {
        from: 'Confirmación Cuenta <notifacion@dentistpocket.com>',
        to: email,
        subject: `Bienvenido ${pnombre}`,
        html: `
            <table border="0" cellpadding="0" cellspacing="0" width="600px" background-color="#2d3436" bgcolor="#2d3436">
            <tr height="200px">
			    <td>
			       <center> 
				       <img src="cid:imageT" alt="logo" height="52" width="100"/>  
				   </center>
			    </td>
                <td bgcolor="#d3df4d">
                    <h1 style="color: #fff; text-align:center">Bienvenido a Dentist Pocket</h1>
                    <p  style="color: #fff; text-align:center">
                        <span style="color: #000"> <b> ${pnombre}, </b> </span> 
                        este es su código de activación:
                    </p>
					<p  style="color: #000; text-align:center; font-size:20px">
				       	<b> ${code} </b>
                    </p>
                </td>
            </tr>
            <tr bgcolor="#fff">
                <td style="text-align:center">
                    <p style="color: #000">¡Un mundo de servicios a su disposición!</p>
                </td>
            </tr>
            </table>        
        `,
		attachments: [{
			filename: 'logo.png',
			path: path.join(__dirname, '../public/base/logo.png'),
			cid: 'imageT'
		}]
    };

	transporter.sendMail(mail_options, (error, info) => {
        if (error) {
            return false
        } else {
            return true
        }
    });
};

userCtrl.sendNotification = (req, res) => {

	const { token, company, body } = req.query

	let message = {
		notification: {
		   body,
	       title: `${company} App`,
		   image: "https://dentistpocket.showupweb.com/base/icon.png"
		},
		token
	}

	admin.messaging().send(message).then(rest => {
		res.status(200).json({ msn: "OK", token })
	}).catch(error => console.log({ error }));
}

userCtrl.renderGetNot = (req, res) => {
	/*const code = Math.floor(Math.random() * 9999)*/
	twilio.messages.create({
			body: `ABRACOL WORKFFLOW te hace la vida más facil. se le informa que tiene el caso 1844 referente a la aprobación masiva de compras nacionales`,
			from: '+12016056221',
			to: '+573116340252'
		}).then(message => res.status(200).json({
			title: "Crear Usuarios",
			msg: message.sid
		}))
		.catch(error => console.log({
			error
		}));

}

userCtrl.renderGetAllPatient = (req, res) => {
	const tTor = []
	const {
		company
	} = req.query

	User.findAll({
			attributes: ['userID', 'userName', 'lastName', 'email', 'pathURL', 'fcmToken'],
			where: {
				company,
				role: 'patient'
			}
		})
		.then(user => {
			user.forEach((value, index) => {
				tTor.push({
					key: index,
					id: value.dataValues.userID,
					title: value.dataValues.userName + " " + value.dataValues.lastName,
					details: value.dataValues.email,
					img: value.dataValues.pathURL,
					fcmToken: value.dataValues.fcmToken
				})
			});
			res.status(200).json({
				data: tTor
			})
		})
		.catch(err => {
			res.status(500).json({
				data: err
			})
		})
}

userCtrl.renderGetByIDPatient = (req, res) => {
	const tTor = []
	const { company, userID} = req.query

	User.findAll({
			attributes: ['userID', 'userName', 'email', 'pathURL', 'resaleID', 'phone', 'city', 'adress', 'age', 'lastName', 'comment'],
			where: { company, role: 'patient', userID }
		})
		.then(user => {
			user.forEach((value, index) => {
				tTor.push({
					key: index,
					userID: value.dataValues.userID,
					userName: value.dataValues.userName,
					lastName: value.dataValues.lastName,
					email: value.dataValues.email,
					pathURL: value.dataValues.pathURL,
					resaleID: value.dataValues.resaleID,
					phone: value.dataValues.phone,
					city: value.dataValues.city,
					adress: value.dataValues.adress,
					age: value.dataValues.age,
					comment: value.dataValues.comment
				})
			});
			res.status(200).json({
				data: tTor
			})
		})
		.catch(err => {
			res.status(500).json({
				'data': err
			})
		})
}

userCtrl.renderGetNew = (req, res) => {
	var errors = []

	const {
		userName, email,userID,
		phone, active, resaleID,
		password, confirm, company, tipo
	} = req.body
	
	if( tipo != 'c' && tipo != 'w' ) {
	  req.body.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
	} else
	  req.body.authCode = nanoid(5)
	  

    if( !active ) {
		if (!userName || !email || !phone) {
			errors.push({ data: 'Ingrese los campos requeridos'	});
		}
	} else {
		if (!password || !confirm) {
			errors.push({ data: 'Ingrese la contraseña' });
		}
		else if (password != confirm) {
			errors.push({ data: 'Las contraseñas no coinciden' });
		}
	}

	if (errors.length > 0)
	  res.status(500).json({ data: errors })
	else {
			User.findAll({
				attributes: ['userID', 'userName', 'email'],
				where: { company,
				   [Op.or]: [
					{ email },
					{ resaleID }
				  ]
			    }
			}).then(dat => 
			{
				if( dat.length > 0 && parseInt(userID) > 0) {
					delete req.body.authCode
					
				    if(!active) {
					 delete req.body.password
				    }

                    User.upsert(req.body).then((data) => {
						res.status(200).json({
							data: { msg: "Usuario actualizado con exito ", id: data[0].dataValues.userID }
						});
					}).catch(err => {
						res.status(500).json({
							data: 'La cuenta de correo ya esta registrada'
						});
					})
				} else if( dat.length == 0 && userID == 0) {
					req.body.userID = 0					
                    User.create(req.body).then((data) => {

						if(tipo == 'c') {
						  enviar_mail(req.body.userName + " " + req.body.lastName, req.body.authCode, req.body.email)
						}

						res.status(200).json({
							data: { msg: "Usuario creado con exitos " + req.body.email, id: data.dataValues.userID, auth: req.body.authCode }
						});
					}).catch(err => {
						res.status(500).json({
							data: 'La cuenta de correo ya esta registrada'
						});
					})
				} else {
					res.status(500).json({
						data: 'La cuenta de correo ya esta registrada'
					});
				}
			}).catch(err => {
				res.status(500).json({
					data: 'La cuenta de correo ya esta registrada'
				});
			})
	}
}

userCtrl.renderGetNewDoc = (req, res) => {
	var errors = []

	const {
		userName,
		lastName,
		email,
		phone,
		city,
		adress,
		active, 
		resaleID,
		company,
		register,
		consultingrooms,
	} = req.body
		
	req.body.authCode = nanoid(5)
	req.body.role = "doctor"
	  
    if( !active ) {
		if (!userName || !email || !phone || !resaleID || !company || consultingrooms <= 0 || !register) {
			errors.push({ data: 'Ingrese los campos requeridos'	});
		}
	}

	if (errors.length > 0)
	  res.status(500).json({ data: errors })
	else 
	{
		req.body.userID = 0
		User.findAll({
			attributes: ['userID', 'userName', 'email'],
			where: { company,
			   [Op.or]: [
				{ email },
				{ resaleID }
			  ]
			}
		}).then((user) => 
		{
			if( user.length <= 0 ) {
				User.create(req.body).then((data) =>  {
					enviar_mail(req.body.userName + " " + req.body.lastName, req.body.authCode, req.body.email)
					res.status(200).json({
						msg: "Usuario creado con exitos " + req.body.email, id: data.dataValues.userID, auth: req.body.authCode, status: true
					});
				})
			}
			else {
				res.status(200).json({
					msg: 'La cuenta de correo ya esta registrada',
					status: false
				});
			}
		}).catch(err => {
			res.status(500).json({
				data: err
			});
		})
	}
}

userCtrl.renderPassword = (req, res) => { 
    req.body.authCode = nanoid(5)
    
    User.upsert(req.body).then((data) => {
		res.status(200).json({
			data: { msg: "Usuario actualizado con exito ", id: data[0].dataValues.userID, auth: req.body.authCode }
		});
	}).catch(err => {
		res.status(500).json({
			data: 'La cuenta de correo ya esta registrada'
		});
	})
}

userCtrl.renderUpload = (req, res) => {

	let EDFile = req.files.file
	
	if (!existsSync(path.join(__dirname, '../public/img/', req.body.user))) {
		mkdirSync(path.join(__dirname, '../public/img/', req.body.user), {
			recursive: true
		})
	}

	EDFile.mv(`${path.join(__dirname,'../','/public/img/',req.body.user,'/')}${req.body.user}.png`, err => {
		if (err) return res.status(500).send({
			data: err
		})

		return res.status(200).send({
			data: 'File upload'
		})
	})
}

userCtrl.renderUploadMark = (req, res) => {

	let EDFile = req.files.file
	
	if (!existsSync(path.join(__dirname, '../public/base/', req.body.company))) {
		mkdirSync(path.join(__dirname, '../public/base/', req.body.company), {
			recursive: true
		})
	}

	EDFile.mv(`${path.join(__dirname,'../','/public/base/',req.body.company,'/')}${req.body.company}.png`, err => {
		if (err)
		 return res.status(200).send({ data: err })

		return res.status(200).send({
			data: 'File upload'
		})
	})
}

userCtrl.renderLogin = (req, res) => {

	const {
		password,
		email,
		token
	} = req.body

	//console.log( token );

	User.findOne({ where: { email } }).then(user =>
	{
		if (!user) {
			res.status(500).json({
				data: "User Not Found"
			})
		}
		else if (!bcrypt.compareSync(password, user.password)) {
			res.status(500).json({
				data: 'Incorrect Password.'
			});
		}
		else {
			User.update({ fcmToken: token }, { where: { email, resaleID: user.resaleID } } )
			.then((rp) => { res.status(200).json({ data: user }); })
			.catch(err => { res.status(500).json({ data: err });  })
		}
	})
	.catch(err => {
		console.log(err );
		res.status(500).json({
			data: err
		})
	})
}

userCtrl.renderGetByToken = (req, res) => {
	const tTor = []
	const { authCode } = req.body

	User.findAll({
			attributes: ['company', 'userID', 'userName', 'email', 'pathURL', 'resaleID', 'role',
			              [User.sequelize.literal('(SELECT fcmToken FROM userFile usr Where usr.company = userFile.company And usr.role = "doctor" limit 0,1)'), 'fcmToken']
			            ],
			where: {
				authCode //,role: 'patient'
			}
		})
		.then(user => {
			if( user.length > 0 )
            {
				user.forEach((value, index) => {
					tTor.push({
						key: index,
						id: value.dataValues.userID,
						title: value.dataValues.userName,
						email: value.dataValues.email,
						resaleID: value.dataValues.resaleID,
						company: value.dataValues.company,
						userName: value.dataValues.userName,
						role: value.dataValues.role,
						fcmToken: value.dataValues.fcmToken
					})
				});
				res.status(200).json({ data: tTor })
			}
			else {
				res.status(500).json({ data: "El código no registra" })
			}
		})
		.catch(err => {
			console.log( err );
			res.status(500).json({
				'data': err
			})
		})
}

module.exports = userCtrl;